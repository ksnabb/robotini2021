use std::{
    collections::VecDeque,
    io::{stdout, Stdout},
};

use opencv::{
    core::{
        bitwise_and, bitwise_or, compare, count_non_zero, merge, no_array, normalize, split,
        Point_, Rect_, Scalar_, Size, BORDER_CONSTANT, CMP_EQ, NORM_MINMAX,
    },
    highgui, imgcodecs,
    imgproc::{
        cvt_color, erode, get_structuring_element, morphology_default_border_value, threshold,
        COLOR_BGR2GRAY, LINE_8, MORPH_RECT, THRESH_BINARY,
    },
    prelude::*,
    types::{VectorOfMat, VectorOfu8},
};

mod connection;
use connection::{Command, Connection, LoginMessage};
use termion::screen::AlternateScreen;
use tui::{
    backend::TermionBackend,
    layout::{Constraint, Direction, Layout},
    widgets::{Block, Borders, List, ListItem, Sparkline},
    Terminal,
};

struct Config {
    pub save_images: bool,
    pub log_verbose: bool,
    pub gui: bool,
    pub tui: bool,
    pub team_id: String,
    pub address: String,
}

impl Config {
    fn from_env() -> Config {
        Config {
            save_images: !std::env::var("DEBUG_IMAGES")
                .unwrap_or(String::from(""))
                .is_empty(),
            log_verbose: !std::env::var("DEBUG_VERBOSE")
                .unwrap_or(String::from(""))
                .is_empty(),
            gui: !std::env::var("DEBUG_GUI")
                .unwrap_or(String::from(""))
                .is_empty(),
            tui: !std::env::var("DEBUG_TUI")
                .unwrap_or(String::from(""))
                .is_empty(),
            team_id: std::env::var("teamid").unwrap_or(String::from("rust")),
            address: std::env::var("SIMULATOR").unwrap_or(String::from("127.0.0.1:11000")),
        }
    }
}

fn save_frame(config: &Config, mat: &Mat, i: usize) -> anyhow::Result<()> {
    let image_name = format!("captures/frame{:04}.png", i);
    save_frame_to_file(config, &image_name, mat)
}

fn save_frame_to_file(config: &Config, name: &str, mat: &Mat) -> anyhow::Result<()> {
    if !config.save_images {
        return Ok(());
    }

    imgcodecs::imwrite(&name, &mat, &opencv::core::Vector::<i32>::new())?;
    Ok(())
}

fn run() -> anyhow::Result<()> {
    std::fs::create_dir_all("captures/debug")?;

    let config = Config::from_env();

    let mut connection = Connection::connect(
        &config.address,
        &LoginMessage {
            name: "Natiivi ja Nopea",
            color: "#ff9514",
            team_id: &config.team_id,
            texture: "https://stuff.h4x0rb34.rs/robotini.png",
        },
    )?;

    if config.gui {
        highgui::named_window("robotini B", 1)?;
        // highgui::named_window("robotini G", 1)?;
        // highgui::named_window("robotini R", 1)?;
    }

    let mut frame_i = 0;
    let mut car_state = CarState {
        speed: 0.0,
        wheels_turn: 0.0,
        previous_horizons: VecDeque::new(),
        pixel_speed: 0.0,
        previous_blue_count: 0.0,
        previous_red_count: 0.0,
        previous_green_count: 0.0,
        command_history: VecDeque::new(),
        previous_center: Some(0.0),
        previous_range: None,
        previous_centers: VecDeque::new(),
    };

    let mut terminal = if config.tui {
        let stdout = AlternateScreen::from(stdout());
        let backend = TermionBackend::new(stdout);
        Some(Terminal::new(backend)?)
    } else {
        None
    };

    loop {
        let image = connection.read_next_image()?;

        let frame =
            opencv::imgcodecs::imdecode(&VectorOfu8::from(image), opencv::imgcodecs::IMREAD_COLOR)?;

        save_frame(&config, &frame, frame_i)?;
        frame_update(&config, &frame, &mut car_state, &mut &mut connection)?;

        if config.tui {
            if let (Some(mut terminal), true) = (terminal.as_mut(), frame_i % 10 == 0) {
                render_debug_tui(&mut terminal, &car_state);
            }
        }

        if config.gui {
            let key = highgui::wait_key(10)?;
            if key > 0 && key != 255 {
                break;
            }
        }

        frame_i += 1;
    }
    Ok(())
}

#[derive(Debug)]
struct CarState {
    wheels_turn: f32,
    speed: f32,
    previous_horizons: VecDeque<i32>,
    pixel_speed: f32,
    previous_blue_count: f32,
    previous_red_count: f32,
    previous_green_count: f32,
    command_history: VecDeque<String>,
    previous_center: Option<f32>,
    previous_range: Option<(usize, usize)>,
    previous_centers: VecDeque<f32>,
}

fn render_debug_tui(
    terminal: &mut Terminal<TermionBackend<AlternateScreen<Stdout>>>,
    car_state: &CarState,
) {
    terminal
        .draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(2)
                .constraints([Constraint::Length(40), Constraint::Min(10)].as_ref())
                .split(f.size());

            let horizon_sparkline_data = car_state
                .previous_horizons
                .iter()
                .rev()
                .map(|horizon| 80 - *horizon as u64)
                .collect::<Vec<_>>();

            let horizon_sparkline = Sparkline::default()
                .block(Block::default().title("Horizon").borders(Borders::ALL))
                .data(&horizon_sparkline_data)
                .max(80);
            f.render_widget(horizon_sparkline, chunks[0]);

            let command_log = car_state
                .command_history
                .iter()
                .map(|command| ListItem::new(command.clone()))
                .collect::<Vec<_>>();
            let command_log_list = List::new(command_log);
            f.render_widget(command_log_list, chunks[1]);
        })
        .unwrap();
}

fn frame_update(
    config: &Config,
    frame: &Mat,
    state: &mut CarState,
    connection: &mut Connection,
) -> anyhow::Result<()> {
    let width = frame.cols();
    let height = frame.rows();

    let wheels_turn = &mut state.wheels_turn;
    let speed = &mut state.speed;

    let (track_mask, blue, green, red, combined) = process_frame(&config, frame)?;

    let (mut horizon_i, _horizon_blackness) = {
        let filtered = &track_mask;
        let cols = filtered.cols();

        (0..filtered.rows())
            .skip(40)
            .map(|y| {
                let row = filtered.at_row::<u8>(y).unwrap();
                let black_pixel_count = row.iter().filter(|px| **px == 0).count();
                let fullness = black_pixel_count as f32 / cols as f32;
                (y, fullness)
            })
            .max_by(|(_, fullness_a), (_, fullness_b)| {
                PartialOrd::partial_cmp(fullness_a, fullness_b).unwrap()
            })
    }
    .unwrap();

    horizon_i = height - horizon_i;

    state.previous_horizons.push_front(horizon_i);
    state.previous_horizons.truncate(60);
    let horizon_interpolated = (state.previous_horizons.iter().sum::<i32>() as f32
        / state.previous_horizons.len() as f32) as i32;
    let (range, blue_found_row) = find_center(height, &track_mask, 30);

    let center = match range {
        None => None,
        Some((l, r)) => {
            let length = r as f32 - l as f32;
            Some(l as f32 + length / 2.0)
        }
    };

    let center_roi_rect = Rect_ {
        x: width / 2 - 10,
        y: 25,
        width: 25,
        height: 15,
    };

    let front_red = Mat::roi(&red, center_roi_rect)?;
    let front_red_count = count_non_zero(&front_red)?;
    let front_green = Mat::roi(&green, center_roi_rect)?;
    let front_green_cont = count_non_zero(&front_green)?;
    let front_blue = Mat::roi(&blue, center_roi_rect)?;
    let front_blue_count = count_non_zero(&front_blue)?;

    state.command_history.push_front(format!(
        "Front R: {:.4} G: {:.4} B: {:.4}",
        front_red_count, front_green_cont, front_blue_count
    ));

    state.command_history.push_front(format!(
        "Horizon interplated, {} (right now {})",
        horizon_interpolated, horizon_i
    ));

    match center {
        Some(center) => {
            state.previous_centers.push_front(center);
            state.previous_centers.truncate(3);
        }
        None => {}
    }

    let center_lolol =
        state.previous_centers.iter().sum::<f32>() / state.previous_centers.len() as f32;

    let screen_center = width as f32 / 2.0;
    let diff = (screen_center - center_lolol) / screen_center;
    *wheels_turn = (diff * diff.abs() * 1.0).max(-0.9f32).min(0.9f32);
    let min_speed = 0.002;

    let max_speed = (horizon_interpolated as f32 / 30.0) * 0.3 + 0.075;

    let slowness_factor = wheels_turn.abs().powf(2.0) * 0.2;
    let slowness_factor = slowness_factor.max(0.8);
    *speed = (0.16 / slowness_factor).min(max_speed).max(min_speed);

    connection.send(&Command::Forward { value: *speed })?;
    connection.send(&Command::Turn {
        value: *wheels_turn,
    })?;

    state.command_history.truncate(30);

    if config.gui {
        let mut viz_frame = combined;

        if let Some(range) = state.previous_range {
            let (range_start, range_end) = range;

            opencv::imgproc::line(
                &mut viz_frame,
                Point_ {
                    x: range_start as i32,
                    y: blue_found_row,
                },
                Point_ {
                    x: range_end as i32,
                    y: blue_found_row,
                },
                Scalar_([0.0, 0.0, 255.0, 0.5]),
                2,
                LINE_8,
                0,
            )?;
        }

        if let Some(center) = center {
            let (range_start, range_end) = range.unwrap();

            opencv::imgproc::line(
                &mut viz_frame,
                Point_ {
                    x: center as i32,
                    y: 0,
                },
                Point_ {
                    x: center as i32,
                    y: 80,
                },
                Scalar_([1.0, 0.0, 1.0, 1.0]),
                2,
                LINE_8,
                0,
            )?;

            opencv::imgproc::line(
                &mut viz_frame,
                Point_ {
                    x: range_start as i32,
                    y: blue_found_row,
                },
                Point_ {
                    x: range_end as i32,
                    y: blue_found_row,
                },
                Scalar_([50.0, 50.0, 50.0, 0.5]),
                2,
                LINE_8,
                0,
            )?;
        }

        highgui::imshow("robotini B", &viz_frame)?;
        // highgui::imshow("robotini G", &green_roi)?;
        // highgui::imshow("robotini R", &red_roi)?;
    }

    state.previous_center = center;
    state.previous_range = range;

    Ok(())
}

fn find_center(height: i32, track_mask: &Mat, starting_y: i32) -> (Option<(usize, usize)>, i32) {
    let mut range = None;
    let mut blue_found_row = starting_y;
    for y in blue_found_row..height {
        let row = track_mask.at_row::<u8>(y).unwrap();

        let mut first_left = None;
        let mut first_right = None;

        for (x, value) in row.iter().enumerate() {
            if *value > 0 {
                first_left = Some(x);
                break;
            }
        }

        for (x, value) in row.iter().enumerate().rev() {
            if *value > 0 {
                first_right = Some(x);
                break;
            }
        }

        match (first_left, first_right) {
            (Some(l), Some(r)) if (r - l) > 30 => {
                blue_found_row = y;
                range = Some((l, r));
                break;
            }
            _ => {}
        };
    }
    (range, blue_found_row)
}

fn process_frame(config: &Config, frame: &Mat) -> anyhow::Result<(Mat, Mat, Mat, Mat, Mat)> {
    // calculate the blacks
    let mut gray_scaled = frame.clone();
    cvt_color(&frame, &mut gray_scaled, COLOR_BGR2GRAY, 0)?;
    save_frame_to_file(&config, "captures/debug/graycolor.png", &gray_scaled)?;

    let mut blacks = frame.clone();
    threshold(&mut gray_scaled, &mut blacks, 30.0, 255.0, THRESH_BINARY)?;

    let mut eroded_blacks = frame.clone();
    erode(
        &blacks,
        &mut eroded_blacks,
        &get_structuring_element(
            MORPH_RECT,
            Size {
                width: 2,
                height: 2,
            },
            Point_ { x: -1, y: -1 },
        )
        .ok()
        .unwrap(),
        Point_ { x: -1, y: -1 },
        1,
        BORDER_CONSTANT,
        morphology_default_border_value().ok().unwrap(),
    )?;

    let mut normalized = frame.clone();
    normalize(
        &frame,
        &mut normalized,
        0.0,
        255.0,
        NORM_MINMAX,
        -1,
        &no_array()?,
    )?;

    let mut preprosessed_image = frame.clone();
    bitwise_and(
        &normalized,
        &normalized,
        &mut preprosessed_image,
        &eroded_blacks,
    )?;

    let (b, g, r) = split_channels(&preprosessed_image)?;

    let mut max_step_1 = g.clone();
    opencv::core::max(&g, &r, &mut max_step_1)?;
    let mut maxed = g.clone();
    opencv::core::max(&b, &max_step_1, &mut maxed)?;
    fn process_channel(buffer: &Mat, max: &Mat, channel_threshold: f64) -> Mat {
        let mut zero_mask = buffer.clone();
        compare(&buffer, &max, &mut zero_mask, CMP_EQ).unwrap();
        let mut zeroed = buffer.clone();
        bitwise_and(&buffer, &zero_mask, &mut zeroed, &no_array().unwrap()).unwrap();

        let mut eroded = zeroed.clone();
        erode(
            &zeroed,
            &mut eroded,
            &get_structuring_element(
                MORPH_RECT,
                Size {
                    width: 2,
                    height: 2,
                },
                Point_ { x: -1, y: -1 },
            )
            .unwrap(),
            Point_ { x: -1, y: -1 },
            1,
            BORDER_CONSTANT,
            morphology_default_border_value().ok().unwrap(),
        )
        .unwrap();

        let mut thresholded = buffer.clone();
        threshold(
            &eroded,
            &mut thresholded,
            channel_threshold,
            255.0,
            THRESH_BINARY,
        )
        .unwrap();
        thresholded
    }

    let blue = process_channel(&b, &maxed, 50.0);
    let green = process_channel(&g, &maxed, 80.0);
    let red = process_channel(&r, &maxed, 100.0);

    let mut combined = Mat::default().unwrap();
    bitwise_or(&blue, &green, &mut combined, &no_array().unwrap()).unwrap();
    let mut track = Mat::default().unwrap();
    bitwise_or(&combined, &red, &mut track, &no_array().unwrap()).unwrap();

    let mut channels = VectorOfMat::new();
    channels.push(blue.clone());
    channels.push(green.clone());
    channels.push(red.clone());

    let mut actually_combined = frame.clone();
    merge(&channels, &mut actually_combined)?;

    Ok((track, blue, green, red, actually_combined))

    /*save_frame_to_file(
        "captures/debug/blue-0.png",
        &split_frame_processed.get(0).unwrap().0,
    )?;
    save_frame_to_file(
        "captures/debug/blue-1.png",
        &split_frame_processed.get(0).unwrap().1,
    )?;
    save_frame_to_file(
        "captures/debug/green-0.png",
        &split_frame_processed.get(1).unwrap().0,
    )?;
    save_frame_to_file(
        "captures/debug/green-1.png",
        &split_frame_processed.get(1).unwrap().2,
    )?;
    save_frame_to_file(
        "captures/debug/red-0.png",
        &split_frame_processed.get(2).unwrap().0,
    )?;
    save_frame_to_file(
        "captures/debug/red-1.png",
        &split_frame_processed.get(2).unwrap().3,
    )?;*/

    // Ok(split_frame_processed)
}

fn split_channels(image: &Mat) -> Result<(Mat, Mat, Mat), anyhow::Error> {
    let mut channels = VectorOfMat::new();
    channels.push(Mat::default()?);
    channels.push(Mat::default()?);
    channels.push(Mat::default()?);
    split(image, &mut channels)?;
    let b = channels.get(0)?;
    let g = channels.get(1)?;
    let r = channels.get(2)?;
    Ok((b, g, r))
}

fn main() {
    run().unwrap()
}
